package com.store.api.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.store.api.entity.RoleEntity;
import com.store.api.model.RoleModel;

@Repository("RoleRepository")
public interface RoleRepository extends JpaRepository<RoleEntity,Serializable> {
	public abstract RoleEntity findById(long id);
	public abstract void save(RoleModel role);
}
