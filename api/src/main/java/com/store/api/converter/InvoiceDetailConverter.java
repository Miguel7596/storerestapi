package com.store.api.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.store.api.entity.InvoiceDetailEntity;
import com.store.api.model.InvoiceDetailModel;

@Component("InvoiceDetailConverter")
public class InvoiceDetailConverter {

	public List<InvoiceDetailModel> ListConverter(List<InvoiceDetailEntity> eInvoiceDetail){
		List<InvoiceDetailModel> list = new ArrayList<>();
		for(InvoiceDetailEntity invoiceDetail : eInvoiceDetail ) {
			list.add(new InvoiceDetailModel(invoiceDetail));
		}
		return list;
	}
	
	public InvoiceDetailModel Converter(InvoiceDetailEntity invoiceDetail) {
		return new InvoiceDetailModel(invoiceDetail);
	}

}
