package com.store.api.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.store.api.entity.RoleEntity;
import com.store.api.helper.StoreResponse;
import com.store.api.service.RoleService;


@RestController
@RequestMapping("/v1")
public class RoleController {

	@Autowired
	@Qualifier("RoleService")
	RoleService service;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@PutMapping("/role")
	public ResponseEntity<ObjectNode> addRole(@RequestBody @Valid RoleEntity role) {
		logger.info("Creating new role");
		return (StoreResponse.defaultResponse(service.create(role)));
	}
	
	@PostMapping("/role")
	public ResponseEntity<ObjectNode> updateRole(@RequestBody @Valid RoleEntity role){
		logger.info("Updating role");
		return StoreResponse.defaultResponse(service.update(role));
	}
	
	@DeleteMapping("/role/{id}")
	public ResponseEntity<ObjectNode> deleteRole(@PathVariable("id") long id) {
		
		return StoreResponse.defaultResponse(service.delete(id));
	}
	
	@GetMapping(value="/role")
	public ResponseEntity<ObjectNode> getRoles(){
		return StoreResponse.response("Roles", service.get());
	}
	
	@GetMapping(value="/role/{id}")
	public ResponseEntity<ObjectNode> obtenerAfiliacion(@PathVariable("id") long id){
		return StoreResponse.response("Role",service.getById(id));
	}

}
