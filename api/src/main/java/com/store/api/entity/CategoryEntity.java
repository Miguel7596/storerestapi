package com.store.api.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.store.api.model.CategoryModel;

@Table(name = "category")
@Entity

public class CategoryEntity {
	
	@GeneratedValue
	@Id
	private long id;
	private String category;
	private String description;

	public CategoryEntity() {
		
	}
	
	public CategoryEntity(CategoryModel category){
		this.category = category.getCategory();
		this.description = category.getDescription();
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
