package com.store.api.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.store.api.model.ProductModel;

@Table(name = "product")
@Entity
public class ProductEntity {

	@GeneratedValue
	@Id
	private long id;

	@ManyToOne
    @JoinColumn(name = "category_id")
	private CategoryEntity category;
	
	@Column(length = 100)
	private String name;
	
	private int stock;

	private float price;
	
	public ProductEntity() {
		
	}

	public ProductEntity(ProductModel product){
		this.name = product.getName();
		this.stock = product.getStock();
		this.price = product.getPrice();
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public CategoryEntity getCategory() {
		return category;
	}

	public void setCategory(CategoryEntity category) {
		this.category = category;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

}
