package com.store.api.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.store.api.model.InvoiceDetailModel;

@Table(name = "invoice_detail")
@Entity
public class InvoiceDetailEntity {
	@GeneratedValue
	@Id
	private long id;
	
	@ManyToOne
    @JoinColumn(name = "product_id")
	private ProductEntity product;
	
	@ManyToOne
    @JoinColumn(name = "invoice_id")
	private InvoiceEntity invoice;
	
	private int quantity;
	
	private float price;

	public InvoiceDetailEntity() {
		
	}
	
	public InvoiceDetailEntity(InvoiceDetailModel invoiceDetail){
		this.quantity = invoiceDetail.getQuantity();
		this.price = invoiceDetail.getPrice();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public ProductEntity getProduct() {
		return product;
	}

	public void setProduct(ProductEntity product) {
		this.product = product;
	}

	public InvoiceEntity getInvoice() {
		return invoice;
	}

	public void setInvoice(InvoiceEntity invoice) {
		this.invoice = invoice;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

}
