package com.store.api.model;

import com.store.api.entity.UserEntity;

public class UserModel  {
	
	private long id;	
	private String name;	
	private String password;
	private String email;	
	private String card;
	private boolean active;	
	private String phone;
	
	public UserModel() {
		
	}
	
	public UserModel(long id, String name, String password, String email, String card, byte rol, boolean active, String phone) {
		this.id = id;
		this.name = name;
		this.password = password;
		this.email = email;
		this.card = card;
		this.active = active;
		this.phone = phone;
	}
	
	public UserModel(UserEntity user) {	
		this.id = user.getId();
		this.name = user.getName();
		this.password = user.getPassword();
		this.email = user.getEmail();
		this.card = user.getCard();
		this.active = user.isActive();
		this.phone = user.getPhone();
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCard() {
		return card;
	}

	public void setCard(String card) {
		this.card = card;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
}
