package com.store.api.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.store.api.model.UserModel;

@Entity
@Table(name = "user")
@EntityListeners(AuditingEntityListener.class)
public class UserEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private long id;

	@ManyToOne
    @JoinColumn(name = "role_id")
	private RoleEntity role;

	@Column(length = 100)
	private String name;
	
	@Column(length = 60)
	private String password;
	
	@Column(length = 60,unique=true)
	private String email;
	
	@Column(unique = true)
	private String card;
	
	@Column(name="active")
	private boolean active;
	
	@Column(name="phone")
	private String phone;

	public UserEntity(UserModel user){
		this.name = user.getName();
		this.password = user.getPassword();
		this.email = user.getEmail();
		this.card = user.getCard();
		this.active = user.isActive();	
		this.phone = user.getPhone();	
	}
	
	public UserEntity() {
		this.active = false;
	}
	
	public RoleEntity getRole() {
		return role;
	}

	public void setRole(RoleEntity role) {
		this.role = role;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCard() {
		return card;
	}

	public void setCard(String card) {
		this.card = card;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
}
