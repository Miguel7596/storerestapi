package com.store.api.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.store.api.entity.UserEntity;
import com.store.api.model.UserModel;

@Component("UserConverter")
public class UserConverter {

		public List<UserModel> ListConverter(List<UserEntity> eUser){
			List<UserModel> list = new ArrayList<>();
			for(UserEntity user : eUser ) {
				list.add(new UserModel(user));
			}
			return list;
		}
		
		public UserModel Converter(UserEntity user) {
			return new UserModel(user);
		}

}
