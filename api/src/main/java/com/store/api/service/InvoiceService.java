package com.store.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.store.api.converter.InvoiceConverter;
import com.store.api.entity.InvoiceEntity;
import com.store.api.model.InvoiceModel;
import com.store.api.repository.InvoiceRepository;

@Service("InvoiceService")
public class InvoiceService {

	@Autowired
	@Qualifier("InvoiceRepository")
	
	private InvoiceRepository repository;
	
	@Autowired
	@Qualifier("InvoiceConverter")
	private InvoiceConverter converter;
	
	public boolean create(InvoiceEntity invoice){
		try {
			repository.save(invoice);
			return true;
		}catch(Exception e) {
			return false;
		}
	}
	
	public boolean update(InvoiceEntity invoice){
		try {
			repository.save(invoice);
			return true;
		}catch(Exception e) {
			return false;
		}
	}
	
	public boolean delete(long id) {
		try {
			InvoiceEntity invoice = repository.findById(id);
			repository.delete(invoice);
			return true;
		}catch(Exception e) {
			return false;
		}
	}
	
	public List<InvoiceModel> get(){
		return converter.ListConverter(repository.findAll());
	}
	
	public InvoiceModel getById(long id) {
		try {
			return converter.Converter(repository.findById(id));
		}catch(Exception e) {
			return null;
		}
	}
	
	public InvoiceEntity getEntityById(long id) {
		try {
			return repository.findById(id);
		}catch(Exception e) {
			return null;
		}
	}

}
