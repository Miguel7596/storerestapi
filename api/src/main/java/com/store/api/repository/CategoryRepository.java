package com.store.api.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.store.api.entity.CategoryEntity;
import com.store.api.model.CategoryModel;

@Repository("CategoryRepository")
public interface CategoryRepository extends JpaRepository<CategoryEntity,Serializable> {
	public abstract CategoryEntity findById(long id);
	public abstract void save(CategoryModel category);
}
