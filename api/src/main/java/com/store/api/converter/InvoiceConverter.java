package com.store.api.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.store.api.entity.InvoiceEntity;
import com.store.api.model.InvoiceModel;

@Component("InvoiceConverter")
public class InvoiceConverter {

	public List<InvoiceModel> ListConverter(List<InvoiceEntity> eInvoice){
		List<InvoiceModel> list = new ArrayList<>();
		for(InvoiceEntity invoice : eInvoice ) {
			list.add(new InvoiceModel(invoice));
		}
		return list;
	}
	
	public InvoiceModel Converter(InvoiceEntity invoice) {
		return new InvoiceModel(invoice);
	}

}
