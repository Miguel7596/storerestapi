package com.store.api.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.store.api.entity.ProductEntity;

@Repository("ProductRepository")
public interface ProductRepository extends JpaRepository<ProductEntity, Serializable> {
	public abstract ProductEntity findByName(String name);
	public abstract ProductEntity findById(long id);
}
