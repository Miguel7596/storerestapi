package com.store.api.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.store.api.converter.UserConverter;
import com.store.api.model.UserModel;
import com.store.api.repository.UserRepository;

@Service("UserService")
public class UserService implements UserDetailsService {
	
	@Autowired
	@Qualifier("UserRepository")
	
	private UserRepository repository;
	
	@Autowired
	@Qualifier("UserConverter")
	private UserConverter convertidor;

	@Override
	public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {

		UserModel user = convertidor.Converter(repository.findByName(name));
		return new User(Long.toString(user.getId()), user.getPassword(), 
				buildGranted(1) );
	}
	
	public List<GrantedAuthority> buildGranted(int rol){
		
		String[] roles = {"Admin", "Other"};
		List<GrantedAuthority> auths = new ArrayList<>();
		
		for(int i = 0; i < rol ; i++) {
			auths.add(new SimpleGrantedAuthority(roles[i]));
		}
		return auths;
	}
}
