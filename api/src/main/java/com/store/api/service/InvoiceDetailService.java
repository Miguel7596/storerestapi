package com.store.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.store.api.converter.InvoiceDetailConverter;
import com.store.api.entity.InvoiceDetailEntity;
import com.store.api.model.InvoiceDetailModel;
import com.store.api.repository.InvoiceDetailRepository;

@Service("InvoiceDetailService")
public class InvoiceDetailService {

	@Autowired
	@Qualifier("InvoiceDetailRepository")
	
	private InvoiceDetailRepository repository;
	
	@Autowired
	@Qualifier("InvoiceDetailConverter")
	private InvoiceDetailConverter converter;
	
	public boolean create(InvoiceDetailEntity invoiceDetail){
		try {
			repository.save(invoiceDetail);
			return true;
		}catch(Exception e) {
			return false;
		}
	}
	
	public boolean update(InvoiceDetailEntity invoiceDetail){
		try {
			repository.save(invoiceDetail);
			return true;
		}catch(Exception e) {
			return false;
		}
	}
	
	public boolean delete(long id) {
		try {
			InvoiceDetailEntity product = repository.findById(id);
			repository.delete(product);
			return true;
		}catch(Exception e) {
			return false;
		}
	}
	
	public List<InvoiceDetailModel> get(){
		return converter.ListConverter(repository.findAll());
	}
	
	public InvoiceDetailModel getById(long id) {
		try {
			return converter.Converter(repository.findById(id));
		}catch(Exception e) {
			return null;
		}
	}
	
	public InvoiceDetailEntity getEntityById(long id) {
		try {
			return repository.findById(id);
		}catch(Exception e) {
			return null;
		}
	}

}
