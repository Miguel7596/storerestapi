package com.store.api.model;

import com.store.api.entity.InvoiceEntity;

public class InvoiceModel {
	
	private long id;
	private String date;

	public InvoiceModel() {
		
	}
	
	public InvoiceModel(long id, String date) {
		this.id = id;
		this.date = date;
	}
	
	public InvoiceModel(InvoiceEntity invoice) {	
		this.id = invoice.getId();
		this.date = invoice.getDate();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

}
