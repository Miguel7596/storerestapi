package com.store.api.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.store.api.entity.CategoryEntity;
import com.store.api.helper.StoreResponse;
import com.store.api.service.CategoryService;

@RestController
@RequestMapping("/v1")
public class CategoryController {

	@Autowired
	@Qualifier("CategoryService")
	CategoryService service;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@PutMapping("/category")
	public ResponseEntity<ObjectNode> addCategory(@RequestBody @Valid CategoryEntity category) {
		logger.info("Creating new category");
		return (StoreResponse.defaultResponse(service.create(category)));
	}
	
	@PostMapping("/category")
	public ResponseEntity<ObjectNode> updateCategory(@RequestBody @Valid CategoryEntity category){
		logger.info("Updating category");
		return StoreResponse.defaultResponse(service.update(category));
	}
	
	@DeleteMapping("/category/{id}")
	public ResponseEntity<ObjectNode> deleteCategory(@PathVariable("id") long id) {
		
		return StoreResponse.defaultResponse(service.delete(id));
	}
	
	@GetMapping(value="/category")
	public ResponseEntity<ObjectNode> getCategories(){
		return StoreResponse.response("Categories", service.get());
	}
	
	@GetMapping(value="/category/{id}")
	public ResponseEntity<ObjectNode> getCategory(@PathVariable("id") long id){
		return StoreResponse.response("Category",service.getById(id));
	}

}
