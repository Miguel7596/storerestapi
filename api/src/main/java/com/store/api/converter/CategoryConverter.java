package com.store.api.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.store.api.entity.CategoryEntity;
import com.store.api.model.CategoryModel;

@Component("CategoryConverter")
public class CategoryConverter {

	public List<CategoryModel> ListConverter(List<CategoryEntity> eCategories){
		List<CategoryModel> list = new ArrayList<>();
		for(CategoryEntity category : eCategories) {
			list.add(new CategoryModel(category));
		}
		return list;
	}

	public CategoryModel Converter(CategoryEntity category) {
		return new CategoryModel(category);
	}
}
