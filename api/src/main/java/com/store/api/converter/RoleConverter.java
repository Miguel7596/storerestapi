package com.store.api.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.store.api.entity.RoleEntity;
import com.store.api.model.RoleModel;

@Component("RoleConverter")
public class RoleConverter {
	
	public List<RoleModel> ListConverter(List<RoleEntity> eRoles){
		List<RoleModel> list = new ArrayList<>();
		for(RoleEntity role : eRoles) {
			list.add(new RoleModel(role));
		}
		return list;
	}

	public RoleModel Converter(RoleEntity role) {
		return new RoleModel(role);
	}

}
