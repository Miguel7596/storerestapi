package com.store.api.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.store.api.entity.InvoiceDetailEntity;
import com.store.api.helper.StoreResponse;
import com.store.api.service.InvoiceDetailService;

@RestController
@RequestMapping("/v1")
public class InvoiceDetailController {

	@Autowired
	@Qualifier("InvoiceDetailService")
	InvoiceDetailService service;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@PutMapping("/invoicedetail")
	public ResponseEntity<ObjectNode> addInvoiceDetail(@RequestBody @Valid InvoiceDetailEntity invoiceDetail) {
		logger.info("Creating new Invoice Detail");
		return (StoreResponse.defaultResponse(service.create(invoiceDetail)));
		
	}
	
	@PostMapping("/invoicedetail")
	public ResponseEntity<ObjectNode> updateInvoiceDetail(@RequestBody @Valid InvoiceDetailEntity invoiceDetail){
		logger.info("Updating Invoice Detail");
		return StoreResponse.defaultResponse(service.update(invoiceDetail));
	}
	
	@DeleteMapping("/invoicedetail/{id}")
	public ResponseEntity<ObjectNode> deleteInvoiceDetail(@PathVariable("id") long id) {
		
		return StoreResponse.defaultResponse(service.delete(id));
	}
	
	@GetMapping(value="/invoicedetail")
	public ResponseEntity<ObjectNode> getInvoiceDetails(){
		return StoreResponse.response("InvoiceDetails", service.get());
	}
	
	@GetMapping(value="/invoicedetail/{id}")
	public ResponseEntity<ObjectNode> getInvoiceDetail(@PathVariable("id") long id){
		return StoreResponse.response("InvoiceDetail",service.getById(id));
	}
}
