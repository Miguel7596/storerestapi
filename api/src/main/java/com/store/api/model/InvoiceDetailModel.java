package com.store.api.model;

import com.store.api.entity.InvoiceDetailEntity;

public class InvoiceDetailModel {

	private long id;
	private int quantity;
	private float price;
	
	public InvoiceDetailModel(long id, int quantity, float price) {
		this.id = id;
		this.quantity = quantity;
		this.price = price;
	}
	
	public InvoiceDetailModel(InvoiceDetailEntity invoiceDetail) {	
		this.id = invoiceDetail.getId();
		this.quantity = invoiceDetail.getQuantity();
		this.price = invoiceDetail.getPrice();
	}
	
	public InvoiceDetailModel() {
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setCantidad(int quantity) {
		this.quantity = quantity;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

}
