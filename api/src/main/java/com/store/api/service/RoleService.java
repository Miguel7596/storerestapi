package com.store.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.store.api.converter.RoleConverter;
import com.store.api.entity.RoleEntity;
import com.store.api.model.RoleModel;
import com.store.api.repository.RoleRepository;

@Service("RoleService")
public class RoleService {
	@Autowired
	@Qualifier("RoleRepository")
	private RoleRepository repository;
	
	@Autowired
	@Qualifier("RoleConverter")
	private RoleConverter converter;

	public boolean create(RoleEntity role){
		try {
			repository.save(role);
			return true;
		}catch(Exception e) {
			return false;
		}
	}
	
	public boolean update(RoleEntity role){
		try {
			repository.save(role);
			return true;
		}catch(Exception e) {
			return false;
		}
	}
	
	public boolean delete(long id) {
		try {
			RoleEntity role = repository.findById(id);
			repository.delete(role);
			return true;
		}catch(Exception e) {
			return false;
		}
	}
	
	public List<RoleModel> get(){
		return converter.ListConverter(repository.findAll());
	}
	
	public RoleModel getById(long id) {
		try {
			return converter.Converter(repository.findById(id));
		}catch(Exception e) {
			return null;
		}
	}
	
	public RoleEntity getEntityById(long id) {
		try {
			return repository.findById(id);
		}catch(Exception e) {
			return null;
		}
	}

}
