package com.store.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.store.api.converter.ProductConverter;
import com.store.api.entity.ProductEntity;
import com.store.api.model.ProductModel;
import com.store.api.repository.ProductRepository;

@Service("ProductService")
public class ProductService {

	@Autowired
	@Qualifier("ProductRepository")
	
	private ProductRepository repository;
	
	@Autowired
	@Qualifier("ProductConverter")
	private ProductConverter converter;
	
	public boolean create(ProductEntity product){
		try {
			repository.save(product);
			return true;
		}catch(Exception e) {
			return false;
		}
	}
	
	public boolean update(ProductEntity product){
		try {
			repository.save(product);
			return true;
		}catch(Exception e) {
			return false;
		}
	}
	
	public boolean delete(long id) {
		try {
			ProductEntity product = repository.findById(id);
			repository.delete(product);
			return true;
		}catch(Exception e) {
			return false;
		}
	}
	
	public List<ProductModel> get(){
		return converter.ListConverter(repository.findAll());
	}
	
	public ProductModel getById(long id) {
		try {
			return converter.Converter(repository.findById(id));
		}catch(Exception e) {
			return null;
		}
	}
	
	public ProductEntity getEntityById(long id) {
		try {
			return repository.findById(id);
		}catch(Exception e) {
			return null;
		}
	}
}
