package com.store.api.model;

import com.store.api.entity.CategoryEntity;

public class CategoryModel {

	private long id;
	private String category;
	private String description;
	
	public CategoryModel(CategoryEntity category) {
		this.id = category.getId();
		this.category = category.getCategory();
		this.description = category.getDescription();
	}
	
	public CategoryModel(long id, String category, String description) {
		this.id = id;
		this.category = category;
		this.description = description;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
