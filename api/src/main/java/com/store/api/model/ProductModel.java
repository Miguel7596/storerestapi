package com.store.api.model;

import com.store.api.entity.ProductEntity;

public class ProductModel {
	
	private long id;
	private String name;
	private int stock;
	private float price;
	
	public ProductModel(long id, String name, int stock, float price) {
		this.id = id;
		this.name = name;
		this.stock = stock;
		this.price = price;
	}
	
	public ProductModel(ProductEntity product) {	
		this.id = product.getId();
		this.name = product.getName();
		this.stock = product.getStock();
		this.price = product.getPrice();
	}

	public ProductModel() {
		
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

}
