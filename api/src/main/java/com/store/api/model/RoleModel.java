package com.store.api.model;

import com.store.api.entity.RoleEntity;

public class RoleModel {
	private long id;
	private String role;

	public RoleModel(RoleEntity role) {
		this.id = role.getId();
		this.role = role.getRole();
	}

	public RoleModel(long id, String role) {
		this.id = id;
		this.role = role;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
}
