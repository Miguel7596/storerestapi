package com.store.api.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.store.api.entity.InvoiceEntity;

@Repository("InvoiceRepository")
public interface InvoiceRepository  extends JpaRepository<InvoiceEntity, Serializable> {
	public abstract InvoiceEntity findById(long id);
}
