package com.store.api.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.store.api.entity.InvoiceDetailEntity;
import com.store.api.entity.ProductEntity;

@Repository("InvoiceDetailRepository")
public interface InvoiceDetailRepository extends JpaRepository<InvoiceDetailEntity, Serializable> {
	public abstract InvoiceDetailEntity findById(long id);
}
