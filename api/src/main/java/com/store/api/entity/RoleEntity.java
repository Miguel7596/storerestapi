package com.store.api.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.store.api.model.RoleModel;

@Table(name = "role")
@Entity
public class RoleEntity {
	@GeneratedValue
	@Id
	private long id;
	
	@Column(length = 100)
	private String role;
	
	public RoleEntity() {
		
	}
	
	public RoleEntity(RoleModel role){
		this.role = role.getRole();
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

}
