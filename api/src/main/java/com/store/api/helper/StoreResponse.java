package com.store.api.helper;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class StoreResponse {
	
	public static ResponseEntity<ObjectNode> errorResponse(String titulo,String mensaje,HttpStatus status){
		HttpHeaders responseHeaders = new HttpHeaders();
		   ObjectMapper mapper = new ObjectMapper();
		   ObjectNode responseBody = mapper.createObjectNode();
		   responseHeaders.set("Content-Type", "application/json");
		   responseBody.put("titulo", titulo);
		   responseBody.put("mensaje", mensaje);
		   return new ResponseEntity<ObjectNode>(responseBody, responseHeaders, status);
	}
	
	public static ResponseEntity<ObjectNode> simpleResponse(ObjectNode mensaje){
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.set("Content-Type", "application/json");
		return new ResponseEntity<ObjectNode>(mensaje, responseHeaders, HttpStatus.OK);
	}

	public static ResponseEntity<ObjectNode> defaultResponse(boolean state){
		ResponseEntity<ObjectNode> response = (state)?StoreResponse.successResponse():StoreResponse.error(HttpStatus.BAD_REQUEST);
		return response;
	}
	
	public static ResponseEntity<ObjectNode> successResponse() {
		   HttpHeaders responseHeaders = new HttpHeaders();
		   ObjectMapper mapper = new ObjectMapper();
		   ObjectNode responseBody = mapper.createObjectNode();
		   responseHeaders.set("Content-Type", "application/json");
		   responseBody.put("status", "Success");
		   return new ResponseEntity<ObjectNode>(responseBody, responseHeaders, HttpStatus.OK);
		}
	
	public static ResponseEntity<ObjectNode> error(HttpStatus status) {
		HttpHeaders responseHeaders = new HttpHeaders();
		   ObjectMapper mapper = new ObjectMapper();
		   ObjectNode responseBody = mapper.createObjectNode();
		   responseHeaders.set("Content-Type", "application/json");
		   responseBody.put("error", "Request Error");
		   return new ResponseEntity<ObjectNode>(responseBody, responseHeaders, status);
	}

	public static ResponseEntity<ObjectNode> response(String fieldName, Object value) {
		   HttpHeaders responseHeaders = new HttpHeaders();
		   ObjectMapper mapper = new ObjectMapper();
		   ObjectNode responseBody = mapper.createObjectNode();
		   responseBody.set(fieldName, mapper.valueToTree(value));
		   responseHeaders.set("Content-Type", "application/json");
		   return new ResponseEntity<ObjectNode>(responseBody, responseHeaders, HttpStatus.OK);
	}
	
	public static ResponseEntity<ObjectNode> response(Object value) {
		   HttpHeaders responseHeaders = new HttpHeaders();
		   ObjectMapper mapper = new ObjectMapper();
		   ObjectNode responseBody = mapper.createObjectNode();
		   JsonNode node = mapper.valueToTree(value);
		   
		   ObjectNode node2 = mapper.convertValue(node, ObjectNode.class);
		   //responseBody.set(fieldName, mapper.valueToTree(value));
		   responseHeaders.set("Content-Type", "application/json");
		   return new ResponseEntity<ObjectNode>(node2, responseHeaders, HttpStatus.OK);
	}
}
