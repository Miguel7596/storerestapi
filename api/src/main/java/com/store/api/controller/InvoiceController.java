package com.store.api.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.store.api.entity.InvoiceEntity;
import com.store.api.helper.StoreResponse;
import com.store.api.service.InvoiceService;

@RestController
@RequestMapping("/v1")
public class InvoiceController {

	@Autowired
	@Qualifier("InvoiceService")
	InvoiceService service;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@PutMapping("/invoice")
	public ResponseEntity<ObjectNode> addInvoice(@RequestBody @Valid InvoiceEntity invoice) {
		logger.info("Creating new invoice");
		return (StoreResponse.defaultResponse(service.create(invoice)));
	}
	
	@PostMapping("/invoice")
	public ResponseEntity<ObjectNode> updateInvoice(@RequestBody @Valid InvoiceEntity invoice){
		logger.info("Updating invoice");
		return StoreResponse.defaultResponse(service.update(invoice));
	}
	
	@DeleteMapping("/invoice/{id}")
	public ResponseEntity<ObjectNode> deleteInvoice(@PathVariable("id") long id) {
		
		return StoreResponse.defaultResponse(service.delete(id));
	}
	
	@GetMapping(value="/invoice")
	public ResponseEntity<ObjectNode> getInvoices(){
		return StoreResponse.response("Invoices", service.get());
	}
	
	@GetMapping(value="/invoice/{id}")
	public ResponseEntity<ObjectNode> getProduct(@PathVariable("id") long id){
		return StoreResponse.response("Invoice",service.getById(id));
	}

}
