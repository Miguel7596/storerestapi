package com.store.api.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.store.api.entity.ProductEntity;
import com.store.api.model.ProductModel;

@Component("ProductConverter")
public class ProductConverter {

	public List<ProductModel> ListConverter(List<ProductEntity> eProduct){
		List<ProductModel> list = new ArrayList<>();
		for(ProductEntity product : eProduct ) {
			list.add(new ProductModel(product));
		}
		return list;
	}
	
	public ProductModel Converter(ProductEntity product) {
		return new ProductModel(product);
	}

}
