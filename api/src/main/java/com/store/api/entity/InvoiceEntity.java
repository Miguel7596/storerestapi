package com.store.api.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.store.api.model.InvoiceModel;

@Table(name = "invoice")
@Entity
public class InvoiceEntity {

	@GeneratedValue
	@Id
	private long id;
	
	@ManyToOne
    @JoinColumn(name = "user_id")
	private UserEntity user;
	
	@Column(length = 100)
	private String date;
	
	public InvoiceEntity() {
		
	}
	
	public InvoiceEntity(InvoiceModel invoice){
		this.date = invoice.getDate();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

}
