package com.store.api.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.store.api.entity.UserEntity;

@Repository("UserRepository")
public interface UserRepository extends JpaRepository<UserEntity, Serializable> {
	public abstract UserEntity findByName(String name);
	
}
