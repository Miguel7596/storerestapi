package com.store.api.configuration;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

import static java.util.Collections.emptyList;

import java.util.Calendar;

public class JwtUtil {

	private static final String password = "L9r6eJ6OQ83Jxr5EgM$6AJxZ4CAwJGH25MFaY";
	
    // Método para crear el JWT y enviarlo al cliente en el header de la respuesta
    static void addAuthentication(HttpServletResponse res, String username) {
    	Calendar cal = Calendar.getInstance();
    	cal.add(Calendar.DAY_OF_MONTH, 1); // Add 1 day
    	
        String token = Jwts.builder()
            .setSubject(username)
            .claim("campo", "valor")
            .claim("level", "2")
            .claim("opcion", "null")
            .setExpiration(getExpiration())
            // Vamos a asignar un tiempo de expiracion de 35 minutos
            // solo con fines demostrativos en el video que hay al final
            .setExpiration(new Date(System.currentTimeMillis() + 2100000))

            // Hash con el que firmaremos la clave
            .signWith(SignatureAlgorithm.HS512, password)
            .compact();

        //agregamos al encabezado el token
        res.addHeader("Authorization", "Bearer " + token);
        res.addHeader("Access-Control-Expose-Headers", "Authorization");
    }

    private static Date getExpiration() {
		Calendar cal = Calendar.getInstance();
    	cal.add(Calendar.DAY_OF_MONTH, 15); // Add 1 day
    	return cal.getTime();
	}
    
    // Método para validar el token enviado por el cliente
    static Authentication getAuthentication(HttpServletRequest request) {

        // Obtenemos el token que viene en el encabezado de la peticion
        String token = request.getHeader("Authorization");

        // si hay un token presente, entonces lo validamos
        if (token != null) {
            String user;
			try {
				user = Jwts.parser()
				        .setSigningKey(password)
				        .parseClaimsJws(token.replace("Bearer", "")) //este metodo es el que valida
				        .getBody()
				        .getSubject();


	            // Recordamos que para las demás peticiones que no sean /login
	            // no requerimos una autenticacion por username/password
	            // por este motivo podemos devolver un UsernamePasswordAuthenticationToken sin password
	            return user != null ?
	                    new UsernamePasswordAuthenticationToken(user, null, emptyList()) :
	                    null;
	        
			} catch (ExpiredJwtException e) {
				System.out.println("Token expired: " + e.getMessage());
				e.printStackTrace();
			} catch (UnsupportedJwtException e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			} catch (SignatureException e) {
				System.out.println("SignatureException: " + e.getMessage());
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        return null;
    }
}