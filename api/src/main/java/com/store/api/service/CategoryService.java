package com.store.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.store.api.converter.CategoryConverter;
import com.store.api.entity.CategoryEntity;
import com.store.api.model.CategoryModel;
import com.store.api.repository.CategoryRepository;

@Service("CategoryService")
public class CategoryService {
	@Autowired
	@Qualifier("CategoryRepository")
	private CategoryRepository repository;
	
	@Autowired
	@Qualifier("CategoryConverter")
	private CategoryConverter converter;
	
	public boolean create(CategoryEntity category){
		try {
			repository.save(category);
			return true;
		}catch(Exception e) {
			return false;
		}
	}
	
	public boolean update(CategoryEntity category){
		try {
			repository.save(category);
			return true;
		}catch(Exception e) {
			return false;
		}
	}
	
	public boolean delete(long id) {
		try {
			CategoryEntity category = repository.findById(id);
			repository.delete(category);
			return true;
		}catch(Exception e) {
			return false;
		}
	}
	
	public List<CategoryModel> get(){
		return converter.ListConverter(repository.findAll());
	}
	
	public CategoryModel getById(long id) {
		try {
			return converter.Converter(repository.findById(id));
		}catch(Exception e) {
			return null;
		}
	}

	public CategoryEntity getEntityById(long id) {
		try {
			return repository.findById(id);
		}catch(Exception e) {
			return null;
		}
	}
}
