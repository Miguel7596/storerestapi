# StoreRESTApi
The following repository include the crud for a Store application. It includes JPA, Hiberante, Spring Security and MVC.

Run the store.sql script on yout MySQL RDBMS.
The default credential for getting a token with Postman are: name: Miguel and password: abc125
The file application.properties contains the database connection, please check the connection url in order to configure corrently in your environment.
If yout want a PostMan collection with all the endpoints that the repository include, please send an email to mmartinez.developer@gmail.com
 