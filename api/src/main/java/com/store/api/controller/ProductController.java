package com.store.api.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.store.api.entity.ProductEntity;
import com.store.api.helper.StoreResponse;
import com.store.api.service.ProductService;

@RestController
@RequestMapping("/v1")
public class ProductController {

	@Autowired
	@Qualifier("ProductService")
	ProductService service;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@PutMapping("/product")
	public ResponseEntity<ObjectNode> addProduct(@RequestBody @Valid ProductEntity product) {
		logger.info("Creating new product");
		return (StoreResponse.defaultResponse(service.create(product)));
		
	}
	
	@PostMapping("/product")
	public ResponseEntity<ObjectNode> updateProduct(@RequestBody @Valid ProductEntity product){
		logger.info("Updating product");
		return StoreResponse.defaultResponse(service.update(product));
	}
	
	@DeleteMapping("/product/{id}")
	public ResponseEntity<ObjectNode> deleteProduct(@PathVariable("id") long id) {
		
		return StoreResponse.defaultResponse(service.delete(id));
	}
	
	@GetMapping(value="/product")
	public ResponseEntity<ObjectNode> getProducts(){
		return StoreResponse.response("Products", service.get());
	}
	
	@GetMapping(value="/product/{id}")
	public ResponseEntity<ObjectNode> getProduct(@PathVariable("id") long id){
		return StoreResponse.response("Product",service.getById(id));
	}
}